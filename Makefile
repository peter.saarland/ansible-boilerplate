SHELL := bash
.ONESHELL:
#.SILENT:
.SHELLFLAGS := -eu -o pipefail -c
#.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
.DEFAULT_GOAL := help

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

DOCKER_BASE_IMAGE ?= python:3.8.5-slim
DOCKER_IMAGE ?= ansible-boilerplate
DOCKER_SHELLFLAGS ?= run --rm -it --name ansible-boilerplate -v ${PWD}:/ansible -v ${HOME}/.gitconfig:/root/.gitconfig ${DOCKER_IMAGE}
ENVIRONMENT_DIR ?= ${HOME}/.if0/.environments/zero
export DOCKER_BUILDKIT=1

.PHONY: help
help:
>	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# Development 
.PHONY: build
build: ## Development: build local Docker image
> @docker image prune -f
> @docker build --build-arg DOCKER_BASE_IMAGE=${DOCKER_BASE_IMAGE} -t ansible-boilerplate .

.PHONY: dev
dev: .SHELLFLAGS = ${DOCKER_SHELLFLAGS}
dev: SHELL := docker
dev: ## Development: run local Docker image
> @bash

.PHONY: ssh
ssh: ${ENVIRONMENT_DIR}/.ssh/id_rsa ${ENVIRONMENT_DIR}/.ssh/id_rsa.pub
