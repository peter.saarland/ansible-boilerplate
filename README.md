# Ansible Boilerplate
This is an Ansible Boilerplate inside a Container that can be used to quickly solve problems with Ansible, even in automated Environments (CI), by leveraging configuration through Environment Variables.

This is **WIP**.

![ansible-boilerplate.jpg](ansible-boilerplate.jpg)

# Available Configuration
```bash
LOGIN_USER=pi
LOGIN_PASSWORD=raspberry
SSH_PRIVATE_KEY_FILE=~/.ssh/id_rsa
NODES=10.0.0.1,10.0.0.2,...
```

# Roles
- Roles will be installed to /etc/ansible/roles
- Custom Roles can be installed/mounted to /ansible/roles

# Inventory
- Inventory will be parsed from /etc/ansible/inventory
- Custom Inventories can be installed/mounted to /inventory
- Default Inventory is reads `NODES` environment variable and sets `ansible_connection` to `ssh`
- `localhost` with `ansible_connection` set to `local` is available implicitly