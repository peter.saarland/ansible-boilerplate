ARG DOCKER_BASE_IMAGE
#FROM ${DOCKER_BASE_IMAGE:-python:3.8.2-slim} as base
FROM ${DOCKER_BASE_IMAGE:-python:3.8.5-slim} as base

FROM base as builder

RUN pip3 install --user --no-cache-dir \
    ansible==2.9 \
    jsondiff \
    docker \
    ansible-lint \
    molecule[lint] \
    docker-compose

FROM base

COPY --from=builder /root/.local /usr/local

LABEL maintainer="Fabian Peter <fabian@peter.saarland>"

ARG DOCKER_IMAGE
ARG SHIPMATE_AUTHOR_URL
ARG SHIPMATE_AUTHOR
ARG SHIPMATE_BUILD_DATE
ARG SHIPMATE_CARGO_VERSION
ARG SHIPMATE_COMMIT_ID

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date="$SHIPMATE_BUILD_DATE"
LABEL org.label-schema.name="$DOCKER_IMAGE"
LABEL org.label-schema.vendor="$SHIPMATE_AUTHOR"
LABEL org.label-schema.url="$SHIPMATE_AUTHOR_URL"
LABEL org.label-schema.version="$SHIPMATE_CARGO_VERSION"
LABEL org.label-schema.vcs-ref="$SHIPMATE_COMMIT_ID"

ENV SHIPMATE_CARGO_VERSION=${SHIPMATE_CARGO_VERSION}

USER root

RUN mkdir -p /root/.ansible/tmp /root/.ansible/cache /ansible /ansible/roles /inventory /roles /collections /environment /etc/ansible /root/.ssh

WORKDIR /usr/local

RUN apt-get -y update --allow-releaseinfo-change && apt-get install -y --no-install-recommends \
    git \
    curl \
    tar \
    unzip \
    make \
    openssh-client \
    sshpass \
    nano \
    jq \
    apache2-utils \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY ansible.cfg /etc/ansible/ansible.cfg

WORKDIR /ansible
RUN echo 'Ansible refuses to read from a world-writeable folder, hence' \
    && chmod -v 700 $(pwd)
    
